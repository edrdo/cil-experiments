
int f1() {
 return 1 + f2(1) + f3();
}

int f2(int x) {
 return f3() + f1();
}

int f3() {
 return f1();
}
