open Cil
open Feature 

let counter = ref 0

class instrCalls = object(self)
  inherit nopCilVisitor
  method vinst (i : instr) =
  match i with
  | Call(lval,Lval(Var(v),NoOffset),list,loc) 
    -> 
       Errormsg.log "%s:%d.%d %s\n" loc.file loc.line loc.byte v.vname;
       incr counter; 
       ChangeTo [Call(lval,
                      Lval(Var(copyVarinfo v (v.vname^"x")), NoOffset),
                      Const(CStr(loc.file)) :: Cil.integer(loc.line) :: list,
                      loc)] 
  | _ -> DoChildren
end

let feature : Feature.t = {
    fd_name = "instrCalls";
    fd_enabled = false; 
    fd_description = "instrument function calls";
    fd_extraopt = [];
    fd_doit = (function f ->
      visitCilFileSameGlobals (new instrCalls) f;
      Errormsg.log "%s contains %d function calls\n" f.fileName !counter;
    );
    fd_post_check = true;
  } 

let () = Feature.register feature
