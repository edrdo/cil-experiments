(* A sample CIL plugin *)

(* API changes from 1.7.3 are marked with XXX below *)

open Cil
open Feature (* XXX you need to open the Feature module *)

class myVisitor = object(self)
  inherit nopCilVisitor

  method vfunc(f : fundec) = 
    f.svar.vname <- "x" ^ f.svar.vname;
    Errormsg.log "New name: %s\n" f.svar.vname;
    SkipChildren
end

(* XXX Cil.featureDescr is now Feature.t *)
let feature : Feature.t = {
    fd_name = "xFunctionNames";
    fd_enabled = false; (* XXX fd_enabled is now a bool, not a bool ref anymore. *)
    fd_description = "count and display the number of functions and function calls";
    fd_extraopt = [];
    fd_doit = (function f ->
      visitCilFileSameGlobals (new myVisitor) f;
    );
    fd_post_check = true;
  } 

(* XXX you need to register each feature using Feature.register. *)
let () = Feature.register feature
