(* A sample CIL plugin *)

(* API changes from 1.7.3 are marked with XXX below *)

open Cil
open Feature (* XXX you need to open the Feature module *)

let callCounter = ref 0
let funcCounter = ref 0

class countCalls = object(self)
  inherit nopCilVisitor

  method vfunc = function
    | _ -> incr funcCounter; DoChildren

  method vinst = function
  | Call _ -> incr callCounter; DoChildren
  | _ -> DoChildren
end

(* XXX Cil.featureDescr is now Feature.t *)
let feature : Feature.t = {
    fd_name = "countFunctionsAndCalls";
    fd_enabled = false; (* XXX fd_enabled is now a bool, not a bool ref anymore. *)
    fd_description = "count and display the number of functions and function calls";
    fd_extraopt = [];
    fd_doit = (function f ->
      visitCilFileSameGlobals (new countCalls) f;
      Errormsg.log "%s contains %d functions and %d function calls\n" f.fileName !funcCounter !callCounter;
    );
    fd_post_check = true;
  } 

(* XXX you need to register each feature using Feature.register. *)
let () = Feature.register feature
